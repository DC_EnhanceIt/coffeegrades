package com.coolcats.myapplication.model.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.coolcats.myapplication.model.data.Coffee;

public class CoffeeDatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "cofffee.db";
    public static String TABLE_NAME = "cofffee";
    public static int DATABASE_VERSION = 1;

    public static String COFFEE_ID = "cofffee_id";
    public static String COFFEE_GRADE_COLUMN = "cofffee_grade";
    public static String COFFEE_TYPE_COLUMN = "coffee_type";


    public CoffeeDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_QUERY = "CREATE TABLE " + TABLE_NAME + " (" + COFFEE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COFFEE_GRADE_COLUMN + " TEXT, " + COFFEE_TYPE_COLUMN + " TEXT)";
        sqLiteDatabase.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int currentVersion, int upgradedVersion) {
        String DROP_TABLE = "DROP TABLE " + TABLE_NAME;
        sqLiteDatabase.execSQL(DROP_TABLE);
        onCreate(sqLiteDatabase);
    }


    public void insertNewCoffee(Coffee coffee) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(COFFEE_GRADE_COLUMN, coffee.getCoffeeGrade());
        contentValues.put(COFFEE_TYPE_COLUMN, coffee.getCoffeeType());
        SQLiteDatabase sqlDb = getWritableDatabase();
        sqlDb.insert(TABLE_NAME, null, contentValues);
    }

    public Cursor getAllCoffee() {
        SQLiteDatabase sqlDB = getReadableDatabase();
        String getCoffee = "SELECT * FROM " + TABLE_NAME;
        return sqlDB.rawQuery(getCoffee, null);
    }
}
