package com.coolcats.myapplication.view;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Toast;

import com.coolcats.myapplication.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.click_me)//going back to the previous activity
    public void clickMe(View view) {
//        finish();
        onBackPressed();
    }


    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme)).setTitle(R.string.return_text)
                .setMessage(R.string.return_dialog_message)
                .setPositiveButton(R.string.okay, (dialogInterface, i) -> {
                    super.onBackPressed();
                }).setNegativeButton(R.string.no, (dialogInterface, i) -> {
            dialogInterface.dismiss();
        }).create()
                .show();

//        super.onBackPressed();
//        Toast.makeText(this, "Ha", Toast.LENGTH_SHORT).show();
    }
}