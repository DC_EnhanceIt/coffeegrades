package com.coolcats.myapplication.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.coolcats.myapplication.R;
import com.coolcats.myapplication.model.data.Coffee;
import com.coolcats.myapplication.model.db.CoffeeDatabaseHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.coolcats.myapplication.model.db.CoffeeDatabaseHelper.COFFEE_GRADE_COLUMN;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.coffee_viewer_textview)
    TextView coffeeViewerTextView;


    private CoffeeDatabaseHelper coffeeDatabaseHelper;
    private Handler handler;

    private String TAG = "TAG_X";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        handler = new Handler();
        coffeeDatabaseHelper = new CoffeeDatabaseHelper(this);
        Log.d(TAG, "onCreate: " + Thread.currentThread().getName());

        readMyCofffee();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestCallPermission();
        }

    }

    private void readMyCofffee() {

        new Thread() {
            @Override
            public void run() {
                super.run();
                Log.d(TAG, "run: Reading data from DB" + Thread.currentThread().getName());

                StringBuilder coffeInfo = new StringBuilder();
                Cursor coffeeCursor = coffeeDatabaseHelper.getAllCoffee();
                coffeeCursor.moveToFirst();

                while (coffeeCursor.moveToNext()) {
                    String coffeeData = "Type: " + coffeeCursor.getString(coffeeCursor.getColumnIndex(COFFEE_GRADE_COLUMN)) + ", Grade: " + coffeeCursor.getString(coffeeCursor.getColumnIndex(COFFEE_GRADE_COLUMN));
                    coffeInfo.append(coffeeData + "\n");
                }
                coffeeViewerTextView.setText(coffeInfo.toString());
            }
        }.start();
    }

    private void requestCallPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 777);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 777 && permissions[0].equals(Manifest.permission.CALL_PHONE)) {

            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Permisison Granted!", Toast.LENGTH_LONG).show();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CALL_PHONE))
                    requestCallPermission();
                else {
                    showCallPermissionNeeded();
                }
            }
        }

    }

    private void showCallPermissionNeeded() {

        new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AppTheme))
                .setTitle(R.string.permission_required_title)
                .setMessage(R.string.call_permission_message)
                .setPositiveButton(R.string.okay, ((dialogInterface, i) -> {
                    openSettings();
                }))
                .setNegativeButton(R.string.no, ((dialogInterface, i) -> {
                    System.exit(0);
                })).create()
                .show();
    }

    private void openSettings() {
        Intent settingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), "Permissions");
        settingsIntent.setData(uri);
        startActivity(settingsIntent);
    }

    @OnClick({R.id.click_me, R.id.my_dialer})
    public void clickMe(View view) {

        switch (view.getId()) {
            case R.id.click_me:
                startActivity(new Intent(this, MainActivity2.class));
                break;
            case R.id.my_dialer:
                makeCall();
                break;
        }

    }

    private void makeCall() {

        Uri numberUri = Uri.parse("tel:222");
        Intent callIntent = new Intent(Intent.ACTION_CALL, numberUri);
        startActivity(callIntent);
    }

}