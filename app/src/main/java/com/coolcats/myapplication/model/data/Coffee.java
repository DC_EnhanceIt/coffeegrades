package com.coolcats.myapplication.model.data;

public class Coffee {

    private int coffeeID;
    private String coffeeGrade;
    private String coffeeType;

    public Coffee(String coffeeGrade, String coffeeType) {
        this.coffeeGrade = coffeeGrade;
        this.coffeeType = coffeeType;
    }

    public int getCoffeeID() {
        return coffeeID;
    }

    public String getCoffeeGrade() {
        return coffeeGrade;
    }

    public void setCoffeeGrade(String coffeeGrade) {
        this.coffeeGrade = coffeeGrade;
    }

    public String getCoffeeType() {
        return coffeeType;
    }

    public void setCoffeeType(String coffeeType) {
        this.coffeeType = coffeeType;
    }
}
